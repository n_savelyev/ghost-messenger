package com.hse.nikita.ghost_messenger;

import android.app.ActionBar;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.InputStreamReader;

import java.io.PrintWriter;

import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.content.SharedPreferences;

import java.security.GeneralSecurityException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import android.net.ConnectivityManager;
import com.hse.nikita.ghost_messenger.XMPP;
import com.scottyab.aescrypt.AESCrypt;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.chat.ChatManagerListener;

import org.openintents.openpgp.util.OpenPgpApi;
import org.openintents.openpgp.util.OpenPgpServiceConnection;
import org.openintents.openpgp.util.OpenPgpUtils;



public class ChatActivity extends ActionBarActivity {

    private EditText messageET;
    private ListView messagesContainer;
    private Button sendBtn;
    private ChatAdapter adapter;
    public ArrayList<ChatMessage> chatHistory;
    private Intent intent;
    public static XMPP xmpp;
    public static ChatManager cm = ChatManager.getInstanceFor(xmpp.connection);
    public static Chat chat;
    public static final String MY_PREFS_NAME = "prefsFile";

    private boolean mBounded;
    String key;
    String interlocutorName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent = getIntent();
        interlocutorName = intent.getStringExtra(ContactsActivity.EXTRA_MESSAGE.toString());
        chat  = cm.createChat(interlocutorName + "@nikita");
        setContentView(R.layout.activity_chat);

        //chatHistory = new ArrayList<ChatMessage>();
        //adapter = new ChatAdapter(ChatActivity.this, new ArrayList<ChatMessage>());

        //messagesContainer.setAdapter(adapter);

        cm.addChatListener(new ChatManagerListener() {
            @Override
            public void chatCreated (Chat chat, boolean createdLocally){
                chat.addMessageListener(new ChatMessageListener() {
                    @Override
                    public void processMessage(Chat chat, final Message message) {
                        Log.e("HAPPENED: ", message.toString());
                        if (message.getType() == Message.Type.chat || message.getType() == Message.Type.normal) {
                            Log.e("HAPPENED AGAIN: ", message.toString());
                            if (message.getBody() != null ) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        ChatMessage msg = new ChatMessage();
                                        msg.setMessage(message.getBody());
                                        msg.setDate(DateFormat.getDateTimeInstance().format(new Date()));
                                        msg.setMe(false);
                                        msg.setId(1000);
                                        msg.setFrom(message.getFrom());
                                        Log.e("RECEIVED!!!", message.getBody() + " from " + message.getFrom());
                                        //chatHistory.add(msg);
                                        displayMessage(msg);
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
        chat  = cm.createChat(interlocutorName + "@nikita");
        cm.setMatchMode(ChatManager.MatchMode.SUPPLIED_JID);

        initControls();

    }




    private void initControls() {
        messagesContainer = (ListView) findViewById(R.id.messagesContainer);
        messageET = (EditText) findViewById(R.id.messageEdit);
        sendBtn = (Button) findViewById(R.id.chatSendButton);

        TextView meLabel = (TextView) findViewById(R.id.meLbl);
        TextView companionLabel = (TextView) findViewById(R.id.friendLabel);
        RelativeLayout container = (RelativeLayout) findViewById(R.id.container);

        companionLabel.setText(interlocutorName);// Hard Coded
        loadDummyHistory();

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = messageET.getText().toString();
                if (TextUtils.isEmpty(messageText)) {
                    return;
                }
                Message message = new Message();
                ChatMessage chatMessage = new ChatMessage();
                chatMessage.setId(1);//dummy
                chatMessage.setMessage(messageText);
                chatMessage.setDate(DateFormat.getDateTimeInstance().format(new Date()));
                chatMessage.setMe(true);
                chatMessage.setFrom("me");
                messageET.setText("");
                if(key!="NO KEY") {
                    try {
                        message.setBody(AESCrypt.encrypt(key, messageText));
                    } catch (GeneralSecurityException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    message.setBody(messageText);
                }
                message.setType(Message.Type.chat);
                message.setTo(chat.getParticipant());

                displayMessage(chatMessage);
                try {
                    Log.e(chat.getParticipant(), messageText);
                    chat.sendMessage(message);
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void displayMessage(ChatMessage message) {
        //chatHistory.add(message);
        if(message.getFrom()!=null) {
            Log.e("MCHECK!!! CHECK!!!:", message.getMessage()+ "__" + message.getFrom() + "__"
                    + interlocutorName + "@nikita" + "__" + message.getIsme());

            if (message.getFrom().startsWith(interlocutorName + "@nikita") || message.getIsme()) {
                adapter.add(message);
                adapter.notifyDataSetChanged();
                scroll();
            }
        }
    }

    private void scroll() {
        messagesContainer.setSelection(messagesContainer.getCount() - 1);
    }

    private void loadDummyHistory(){

        chatHistory = new ArrayList<ChatMessage>();

        ChatMessage msg = new ChatMessage();
        msg.setId(1);
        msg.setMe(false);
        SharedPreferences prefs = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        String restoredText = prefs.getString(interlocutorName, null);
        key = "NO KEY";
        if (restoredText != null) {
            key = prefs.getString(interlocutorName, "NO KEY");//"No name defined" is the default value.
        }


        msg.setFrom(interlocutorName+"@nikita");
        msg.setMessage("Привет, я " + interlocutorName + ". Мой ключ:" + key);

        msg.setDate(DateFormat.getDateTimeInstance().format(new Date()));
        chatHistory.add(msg);
        ChatMessage msg1 = new ChatMessage();
        msg1.setId(2);
        msg1.setMe(false);
        msg1.setFrom(interlocutorName+"@nikita");
        msg1.setMessage("ТЫ ОК???");
        msg1.setDate(DateFormat.getDateTimeInstance().format(new Date()));

        adapter = new ChatAdapter(ChatActivity.this, new ArrayList<ChatMessage>());

        messagesContainer.setAdapter(adapter);

        for(int i=0; i<chatHistory.size(); i++) {
            ChatMessage message = chatHistory.get(i);
            displayMessage(message);

        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



}