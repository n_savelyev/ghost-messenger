package com.hse.nikita.ghost_messenger;


import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.Toast;
import android.view.View;
import android.widget.ListAdapter;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;

import java.util.Collection;

public class ContactsActivity extends AppCompatActivity {
    SQLiteOpenHelper dbHelper;
    public static XMPP xmpp;
    public final static String EXTRA_MESSAGE = "com.hse.nikita.ghost_messenger.MESSAGE";

    String[] names = {"ADD NEW CONTACT", "ADD NEW CONTACT","ADD NEW CONTACT",
            "ADD NEW CONTACT","ADD NEW CONTACT","ADD NEW CONTACT","ADD NEW CONTACT",
            "ADD NEW CONTACT","ADD NEW CONTACT","ADD NEW CONTACT","ADD NEW CONTACT",
            "ADD NEW CONTACT","ADD NEW CONTACT","ADD NEW CONTACT",
            "ADD NEW CONTACT","ADD NEW CONTACT","ADD NEW CONTACT"};
    int [] images = {R.mipmap.ic_face, R.mipmap.ic_face5, R.mipmap.ic_face4,
            R.mipmap.ic_face2, R.mipmap.ic_face, R.mipmap.ic_face2,
            R.mipmap.ic_face3, R.mipmap.ic_face1, R.mipmap.ic_face5,
            R.mipmap.ic_face4, R.mipmap.ic_face1,
            R.mipmap.ic_face4, R.mipmap.ic_face1,
            R.mipmap.ic_face4, R.mipmap.ic_face1 };



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        SlidingMenu menu = new SlidingMenu(this);

        menu.setMode(SlidingMenu.LEFT);
        //menu.setStatic(true);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setFadeDegree(0.6f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        menu.setMenu(R.layout.list_sidemenu);
        Roster roster = Roster.getInstanceFor(XMPP.getInstance().connection);

        if (!roster.isLoaded())
            try {
                roster.reloadAndWait();
            } catch (SmackException.NotLoggedInException e) {
                e.printStackTrace();
            } catch (SmackException.NotConnectedException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        String[] groups = {"1"};

        Collection<RosterEntry> entries = roster.getEntries();
        int i=0;
        if(!entries.isEmpty()) {
            for (RosterEntry entry : entries) {
                Log.e("Here: ", entry.toString());
                if(entry.getName()!=null && !entry.getName().isEmpty())
                    names[i] = entry.getName().toString();
                i++;
            }
        }


        menu.setBehindWidthRes(R.dimen.slidingmenu_behind_width);

        String[] items = {"Notifications", "Invisible status", "Encryption"};
        ((ListView) findViewById(R.id.sidemenu)).setAdapter(
                new ArrayAdapter<Object>(
                        this,
                        R.layout.item_sidemenu,
                        R.id.text,
                        items
                )
        );


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        ListView lvContacts = (ListView) findViewById(R.id.listView);

        lvContacts=(ListView) findViewById(R.id.listView);
        lvContacts.setAdapter(new ContactsAdapter(this, names ,images));

        lvContacts.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                String Selecteditem = names[+position];
                if(Selecteditem.equals(getString(R.string.add_new_contact))){
                    InviteFriend();
                }
                else {
                    Log.e(Selecteditem, "ok");
                    LaunchChat(Selecteditem);
                }
                //Toast.makeText(getApplicationContext(), Selecteditem, Toast.LENGTH_SHORT).show();

            }
        });

    }
    private void LaunchChat(String name){
        Intent intent = new Intent(this, ChatActivity.class);
        String message = name;
        Log.e(name, "ok");
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
    private void InviteFriend(){
        Intent intent = new Intent(this, InvitationActivity.class);
        startActivity(intent);
    }
}
