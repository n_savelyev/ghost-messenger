package com.hse.nikita.ghost_messenger;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.roster.Roster;
import android.content.SharedPreferences;
import java.text.DateFormat;
import java.util.Date;
import android.widget.Button;
import android.widget.EditText;

public class InvitationActivity extends AppCompatActivity {
    private Button btn;
    private EditText name;
    private EditText password;
    public static final String MY_PREFS_NAME = "prefsFile";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation);
        Log.e("SHK", "SHJFKSJF");
        initControls();
    }

    private void initControls() {
        name = (EditText) findViewById(R.id.NameInput);
        password = (EditText) findViewById(R.id.KeyInput);
        btn = (Button) findViewById(R.id.button);
        Log.e("SHK", "SHJFKSJF");
        btn.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick (View v){
            String username = name.getText().toString();
            String key = password.getText().toString();
            if (TextUtils.isEmpty(username)) {
                Log.e(username+"SJKLSJSLL", key);
                return;
            }
            else{
                Log.e(username, key);
                SharedPreferences.Editor editor = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
                editor.putString(username, key);
                editor.apply();


                Roster roster = Roster.getInstanceFor(XMPP.getInstance().connection);
                String[] groups = {"1"};
                try {
                    roster.createEntry(username+"@nikita", username, groups);
                } catch (SmackException.NotLoggedInException e) {
                    e.printStackTrace();
                } catch (SmackException.NoResponseException e) {
                    e.printStackTrace();
                } catch (XMPPException.XMPPErrorException e) {
                    e.printStackTrace();
                } catch (SmackException.NotConnectedException e) {
                    e.printStackTrace();
                }
            }

        }
        }

        );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
